<?php
	require_once 'models/Writable.php';
	require_once 'models/Item.php';
	require_once 'models/Bonus.php';
	require_once 'models/Food.php';
    require_once 'models/GoodsItem.php';
    require_once 'ItemsWriter.php';

    $itemWriter = new ItemsWriter();
	$itemWriter->addItem(new Food('Luccy', 50.9));
	$itemWriter->addItem(new Food('Potate', 40.4));
	$itemWriter->addItem(new Food('Shelly', 10.5));
	$itemWriter->addItem(new Food('Mango', 15));
	$itemWriter->addItem(new GoodsItem('Chiip-tea', 70.8));
	$itemWriter->addItem(new Bonus('Накопительный бонус', 'cat4', 'Ашан предлагает вам	 бонуссы.'));
	$total = 0;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>Ashan</title>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-expand nav-light bg-light">
			<a href="index.php" class="navbar-brand">Ashan.com</a>
			<div class="navbar-collapse collapse">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a href="index.php" class="nav-link">Main</a>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link">Contact</a>
					</li>
				</ul>
			</div>
		</nav>
		<div class="card col-5 my-3 mx-auto">
			<div class="card-head">
				<h4 class="card-title text-center mt-3">Bill №2455543</h4>
			</div>
			<div class="card-body">
                <ul>
                    <?php foreach($itemWriter->itemsList as $item): ?>
                        <li class=""><?= $item->getSummaryLine() ?></li>
                        <?php $total += !($item instanceof Bonus) ? $item->getPrice() : 0 ?>
                    <?php endforeach; ?>
                </ul>
                <span class="badge col-12 text-right"><?= 'TOTAL: ' . $total . ' $' ?></span>

			</div>
			<div class="card-footer text-center">
				<a href="" class="btn btn-info">Print</a>
				<a href="" class="btn btn-info">Send to Email</a>
				<a href="" class="btn btn-warning">Exit</a>
			</div>
		</div> <!-- end card -->
		<footer class="navbar navbar-expand nav-light bg-light">
			<a href="index.php" class="navbar-brand">Ashan.com</a>
			<div class="navbar-collapse collapse">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a href="index.php" class="nav-link">Main</a>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link">Contact</a>
					</li>
				</ul>
			</div>
		</footer>
	</div> <!-- end container -->
</body>
</html>
