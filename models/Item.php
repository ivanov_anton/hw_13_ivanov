<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24.03.19
 * Time: 8:34
 */

abstract class Item implements Writable
{
    protected $title;
    protected static $type = 'base_item';
    protected $price;

    public function __construct($title, $price)
    {
        $this->title = $title;
        $this->price = $price;
    }

    public function getTitle()
    {
        return $this->title;
    }

    // type устанавливается вызовом статического метода getType
    public static function getType()
    {
        return static::$type;
    }

    public abstract function getSummaryLine();
    public abstract function getPrice();

}
