<?php
class Food extends Item
{
    protected $price;
    protected static $type = 'food';

    public function getSummaryLine()
    {
        return 'title: ' . $this->getTitle() . ' type: ' . self::getType() . ' price: ' . $this->getPrice();
    }

    public function getPrice()
    {
        return $this->price - 10;
    }
}
