<?php
class Bonus implements Writable
{
    protected $title;
    protected $type;
    protected $descr;

    /**
     * Bonus constructor.
     * @param $title
     * @param $type
     * @param $descr
     */
    public function __construct($title, $type, $descr)
    {
        $this->title = $title;
        $this->type = $type;
        $this->descr = $descr;
    }

    public function getSummaryLine()
    {
        return 'title: ' . $this->getTitle() . ' type: ' . $this->getType() . ' descr: ' . $this->getDescr();
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getDescr()
    {
        return $this->descr;
    }
}
