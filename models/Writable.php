<?php

interface Writable
{
    public function getSummaryLine();
}
