<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 24.03.19
 * Time: 9:01
 */

class GoodsItem extends Item
{
    protected $discount;
    protected static $type = 'goods';

    public function getSummaryLine()
    {
        return 'title: ' . $this->getTitle() . ' type: ' . static::getType() . ' price: ' . $this->getPrice();
    }

    public function getPrice()
    {
        return $this->price - $this->discount;
    }
}
